from email.headerregistry import UniqueUnstructuredHeader


from tkinter import CASCADE


from django.db import models
from django.forms import modelformset_factory


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + " by " + self.author


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField(default=0.00)
    recipe = models.ForeignKey(to='Recipe', related_name="ingredients", on_delete=models.CASCADE)
    measure = models.ForeignKey(to='Measure', on_delete=models.PROTECT)
    fooditem = models.ForeignKey(to='FoodItem', on_delete=models.PROTECT)

    def __str__(self):
        return self.fooditem

    

class Step(models.Model):
    recipe = models.ForeignKey("Recipe", related_name="steps", on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField
    directions = models.CharField(max_length=300)

    def __str__(self):
        return self.directions

    
class Tag(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="tags")
